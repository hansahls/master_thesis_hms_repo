---
title: "Plotting_Alignments"
author: "Simen Rød Sandve, edited by Hanna Sahlström"
date: "12/14/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(DT); library(plotly)
library(seqinr)
library(graphics)
```

Retrieving Alignment for SsalEle0180 after running Phylogenetic_Analysis.rmd. Phylogenetic_Analysis.rmd code must be run prior to plotting the alignments for each test TE.  
```{r}
# SsalEle0180
align_SsalEle0180 <- read.fasta('../Output_data/FASTA/NC_027308.1_24002910-24004524_vs_DTT_Tsn1-2_DNA_TcMar-Tc1.ali')

alilength <- getLength(align_SsalEle0180)[1]


#Plotting the alignment and storing it in the master repo

pdf('../Output_data/Plots/VisualAlignment_SsalEle0180.pdf', paper =  'a4')
#plot empty plot
par(mar=c(4,10,3,4))
plot('', xlim = c(1,alilength), ylim = c(0,3), yaxt='n', frame.plot = F, xlab='alignment positions')

# fill inn with coloured boxes
cols <- c()

for(n in 1:length(align_SsalEle0180)){
  print(n)
  cols <- sub('^n$', 'white', sub('-', 'grey', sub('^t$', 'blue', sub('^g$', 'yellow', gsub('c', 'darkgreen', gsub('a', 'salmon', align_SsalEle0180[[n]]))))))
  
  for(i in 1:alilength){
    print(i)
    rect(xleft = i-0.5, xright = i+0.5, ytop = (ifelse(n==1, 0.2, 0.5)+0.1), 
         ybottom = (ifelse(n==1, 0.2, 0.5)-0.1), col = cols[i], border = NA)
  }
  
}


cols <- ifelse(align_SsalEle0180[[1]]==align_SsalEle0180[[2]], 'white', 'darkred')


for(i in 1:alilength){
  rect(xleft = i-0.5, xright = i+0.5, ytop = 0.32, 
       ybottom = 0.38, col = cols[i], border = NA)
}

mtext(text = names(align_SsalEle0180)[2],side = 2,at = 0.5, las=2, cex=0.7)
mtext(text = names(align_SsalEle0180)[1],side = 2,at = 0.2, las=2, cex=0.7)

# # adding zoom focus: bp 45-67
# rect(xleft = 45, xright = 67, ytop = 0.65, ybottom = 0.05, col = alpha('darkred', 0.5), border = alpha('darkred', 0.5),)

dev.off()

```

Retrieving Alignment for SsalEle0256 after running Phylogenetic_Analysis.rmd. Phylogenetic_Analysis.rmd code must be run prior to plotting the alignments for each test TE.  
```{r}
# SsalEle0256
align_SsalEle0256 <- read.fasta('../Output_data/FASTA/NC_027305.1_63240440-63242031_vs_DTT_SsalEle1069_DNA_TcMar-Tc1.ali')

alilength <- getLength(align_SsalEle0256)[1]


#Plotting the alignment and storing it in the master repo

pdf('../Output_data/Plots/VisualAlignment_SsalEle0256.pdf', paper =  'a4')
#plot empty plot
par(mar=c(4,10,3,4))
plot('', xlim = c(1,alilength), ylim = c(0,3), yaxt='n', frame.plot = F, xlab='alignment positions')

# fill inn with coloured boxes
cols <- c()

for(n in 1:length(align_SsalEle0256)){
  print(n)
  cols <- sub('^n$', 'white', sub('-', 'grey', sub('^t$', 'blue', sub('^g$', 'yellow', gsub('c', 'darkgreen', gsub('a', 'salmon', align_SsalEle0256[[n]]))))))
  
  for(i in 1:alilength){
    print(i)
    rect(xleft = i-0.5, xright = i+0.5, ytop = (ifelse(n==1, 0.2, 0.5)+0.1), 
         ybottom = (ifelse(n==1, 0.2, 0.5)-0.1), col = cols[i], border = NA)
  }
  
}


cols <- ifelse(align_SsalEle0256[[1]]==align_SsalEle0256[[2]], 'white', 'darkred')


for(i in 1:alilength){
  rect(xleft = i-0.5, xright = i+0.5, ytop = 0.32, 
       ybottom = 0.38, col = cols[i], border = NA)
}

mtext(text = names(align_SsalEle0256)[2],side = 2,at = 0.5, las=2, cex=0.7)
mtext(text = names(align_SsalEle0256)[1],side = 2,at = 0.2, las=2, cex=0.7)

# # adding zoom focus: bp 45-67
# rect(xleft = 45, xright = 67, ytop = 0.65, ybottom = 0.05, col = alpha('darkred', 0.5), border = alpha('darkred', 0.5),)

dev.off()

```

Retrieving Alignment for SsalEle0351 after running Phylogenetic_Analysis.rmd. Phylogenetic_Analysis.rmd code must be run prior to plotting the alignments for each test TE.  
```{r}
# SsalEle0351
align_SsalEle0351 <- read.fasta('../Output_data/FASTA/NC_027309.1_7296152-7297281_vs_DTT_SsalEle0351_DNA_TcMar-Tc1.ali')

alilength <- getLength(align_SsalEle0351)[1]


#Plotting the alignment and storing it in the master repo

pdf('../Output_data/Plots/VisualAlignment_SsalEle0351.pdf', paper =  'a4')
#plot empty plot
par(mar=c(4,10,3,4))
plot('', xlim = c(1,alilength), ylim = c(0,3), yaxt='n', frame.plot = F, xlab='alignment positions')

# fill inn with coloured boxes
cols <- c()

for(n in 1:length(align_SsalEle0351)){
  print(n)
  cols <- sub('^n$', 'white', sub('-', 'grey', sub('^t$', 'blue', sub('^g$', 'yellow', gsub('c', 'darkgreen', gsub('a', 'salmon', align_SsalEle0351[[n]]))))))
  
  for(i in 1:alilength){
    print(i)
    rect(xleft = i-0.5, xright = i+0.5, ytop = (ifelse(n==1, 0.2, 0.5)+0.1), 
         ybottom = (ifelse(n==1, 0.2, 0.5)-0.1), col = cols[i], border = NA)
  }
  
}


cols <- ifelse(align_SsalEle0351[[1]]==align_SsalEle0351[[2]], 'white', 'darkred')


for(i in 1:alilength){
  rect(xleft = i-0.5, xright = i+0.5, ytop = 0.32, 
       ybottom = 0.38, col = cols[i], border = NA)
}

mtext(text = names(align_SsalEle0351)[2],side = 2,at = 0.5, las=2, cex=0.7)
mtext(text = names(align_SsalEle0351)[1],side = 2,at = 0.2, las=2, cex=0.7)

# # adding zoom focus: bp 45-67
# rect(xleft = 45, xright = 67, ytop = 0.65, ybottom = 0.05, col = alpha('darkred', 0.5), border = alpha('darkred', 0.5),)

dev.off()

```

Retrieving Alignment for SsalEle0401 after running Phylogenetic_Analysis.rmd. Phylogenetic_Analysis.rmd code must be run prior to plotting the alignments for each test TE.  
```{r}
# SsalEle0401
align_SsalEle0401 <- read.fasta('../Output_data/FASTA/NC_027309.1_7318227-7319384_vs_DTT_SsalEle0401_DNA_TcMar-Tc1.ali')

alilength <- getLength(align_SsalEle0401)[1]


#Plotting the alignment and storing it in the master repo

pdf('../Output_data/Plots/VisualAlignment_SsalEle0401.pdf', paper =  'a4')
#plot empty plot
par(mar=c(4,10,3,4))
plot('', xlim = c(1,alilength), ylim = c(0,3), yaxt='n', frame.plot = F, xlab='alignment positions')

# fill inn with coloured boxes
cols <- c()

for(n in 1:length(align_SsalEle0401)){
  print(n)
  cols <- sub('^n$', 'white', sub('-', 'grey', sub('^t$', 'blue', sub('^g$', 'yellow', gsub('c', 'darkgreen', gsub('a', 'salmon', align_SsalEle0401[[n]]))))))
  
  for(i in 1:alilength){
    print(i)
    rect(xleft = i-0.5, xright = i+0.5, ytop = (ifelse(n==1, 0.2, 0.5)+0.1), 
         ybottom = (ifelse(n==1, 0.2, 0.5)-0.1), col = cols[i], border = NA)
  }
  
}


cols <- ifelse(align_SsalEle0401[[1]]==align_SsalEle0401[[2]], 'white', 'darkred')


for(i in 1:alilength){
  rect(xleft = i-0.5, xright = i+0.5, ytop = 0.32, 
       ybottom = 0.38, col = cols[i], border = NA)
}

mtext(text = names(align_SsalEle0401)[2],side = 2,at = 0.5, las=2, cex=0.7)
mtext(text = names(align_SsalEle0401)[1],side = 2,at = 0.2, las=2, cex=0.7)

# # adding zoom focus: bp 45-67
# rect(xleft = 45, xright = 67, ytop = 0.65, ybottom = 0.05, col = alpha('darkred', 0.5), border = alpha('darkred', 0.5),)

dev.off()

```

Retrieving Alignment for SsalEle0709 after running Phylogenetic_Analysis.rmd. Phylogenetic_Analysis.rmd code must be run prior to plotting the alignments for each test TE.  
```{r}
# SsalEle0709
align_SsalEle0709 <- read.fasta('../Output_data/FASTA/NC_027324.1_32494034-32495493_vs_DTT_DTSsa9-1_DNA_TcMar-Tc1.ali')

alilength <- getLength(align_SsalEle0709)[1]


#Plotting the alignment and storing it in the master repo

pdf('../Output_data/Plots/VisualAlignment_SsalEle0709.pdf', paper =  'a4')
#plot empty plot
par(mar=c(4,10,3,4))
plot('', xlim = c(1,alilength), ylim = c(0,3), yaxt='n', frame.plot = F, xlab='alignment positions')

# fill inn with coloured boxes
cols <- c()

for(n in 1:length(align_SsalEle0709)){
  print(n)
  cols <- sub('^n$', 'white', sub('-', 'grey', sub('^t$', 'blue', sub('^g$', 'yellow', gsub('c', 'darkgreen', gsub('a', 'salmon', align_SsalEle0709[[n]]))))))
  
  for(i in 1:alilength){
    print(i)
    rect(xleft = i-0.5, xright = i+0.5, ytop = (ifelse(n==1, 0.2, 0.5)+0.1), 
         ybottom = (ifelse(n==1, 0.2, 0.5)-0.1), col = cols[i], border = NA)
  }
  
}


cols <- ifelse(align_SsalEle0709[[1]]==align_SsalEle0709[[2]], 'white', 'darkred')


for(i in 1:alilength){
  rect(xleft = i-0.5, xright = i+0.5, ytop = 0.32, 
       ybottom = 0.38, col = cols[i], border = NA)
}

mtext(text = names(align_SsalEle0709)[2],side = 2,at = 0.5, las=2, cex=0.7)
mtext(text = names(align_SsalEle0709)[1],side = 2,at = 0.2, las=2, cex=0.7)

# # adding zoom focus: bp 45-67
# rect(xleft = 45, xright = 67, ytop = 0.65, ybottom = 0.05, col = alpha('darkred', 0.5), border = alpha('darkred', 0.5),)

dev.off()

```
