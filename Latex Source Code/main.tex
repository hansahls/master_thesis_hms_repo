\documentclass[12pt,a4paper]{report}
\renewcommand{\baselinestretch}{1.0}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{gensymb}
\usepackage{lscape}

\usepackage{parskip} %changes format from paragraph indent to a line between paragraphs

\usepackage[margin=2.5cm]{geometry} %sets margins

\usepackage{float} %allows H to be used to hold figures in place

\usepackage{titlesec}
\titleformat{\chapter}% reformat chapter headings
     [hang]% like section, with number on same line
     {\Huge\bfseries}% formatting applied to whole
     {Chapter \thechapter:}% Chapter number
     {0.5em}% space between # and title
     {}% formatting applied just to title
   
\usepackage{appendix}   
     
\bibliographystyle{agsm} %agsm is Harvard style bibliography

\usepackage{natbib} %bibliography package


\title{TRANSPOSABLE ELEMENTS CARRY TRANSCRIPTION FACTOR BINDING SITES THAT EFFECT LIVER SPECIFIC GENE REGULATION IN ATLANTIC SALMON}
\author{Hanna Magdalena Sahlström}


\usepackage{acro}
% probably a good idea for the nomenclature entries:
\acsetup{first-style=short}
% class `abbrev': abbreviations:
\DeclareAcronym{TFBS}{
  short = TFBS ,
  long  = Transcription Factor Binding Site,
  class = abbrev
}

\DeclareAcronym{TE}{
  short = TE ,
  long  = Transposable Element,
  class = abbrev
}

\DeclareAcronym{TSD}{
  short = TSD ,
  long  = Target Site Duplication,
  class = abbrev
}

\DeclareAcronym{WGD}{
  short = WGD ,
  long  = Whole Genome Duplication,
  class = abbrev
}

\DeclareAcronym{TF}{
  short = TF ,
  long  = Transcription Factor,
  class = abbrev
}

\DeclareAcronym{TIR}{
  short = TIR ,
  long  = Terminal Inverted Repeats,
  class = abbrev
}

\DeclareAcronym{ORF}{
  short = ORF ,
  long  = Open Reading Frame,
  class = abbrev
}

\begin{document}

\maketitle

\begin{abstract}
    hello
\end{abstract}

\tableofcontents
\listoffigures
\cleardoublepage
\printacronyms[include-classes=abbrev,name=Abbreviations]

\chapter{Introduction}
Through this chapter, the specific theories concerning this thesis will be thoroughly explored. These include transposable elements acting as promoters for gene regulation, transcription factor binding sites having cis-regulatory influences on neighbouring genes, as well as the molecular processes that occur after whole genome duplication in salmonids. However, in order to lay the groundworks for these specific topics, a more general exploration of the principles of gene regulation, transposable elements and synthetic biology will be covered. Though it is expected that the reader has some previous understanding of these principles, it is prudent to present these concepts to allow further discussion later in the thesis. 

\section{Theory}
\subsection{Background}
DNA is the fundamental instruction manual that determines how your cells will differentiate and allows for us to pass on our hereditary information to our descendents. Since genome sequencing revolutionized the field of genetics, scientists have made groundbreaking progress in understanding how evolution on a molecular level functions. Yet, the complex networks that regulate genes still raise many unanswered questions. The switches that turn on and off transcription form an incredibly complicated system that determines whether or not a gene will be transcribed, and at what rate. One tactic used to study the evolution and regulation of genes is to study a genome after it has undergone whole genome duplication(\ac{WGD}). Observing how molecular systems manage the strain of having doubled the amount of genetic material is useful to observe how cells manage gene dosages, mutations and huge shifts of genetic information. 

 These evolutionary landmarks are a challenge to study, as most WGDs occurred too long ago for clear signs of post-WGD reorganization events to remain\citep{BackgroundArticle}. Examples of such WGDs are 1R and 2R, where lampreys digressed from the jawed vertebrates\citep{BackgroundArticle}. However, salmonids underwent a more recent WGD (Ss4r, salmonid specific autotetraploidization event), that only occurred about 80 million years ago\citep{BackgroundArticle}. This has provided a unique opportunity to study vertebrate genome evolution, in a window of time that is long enough to observe evolutionary paradigms, yet short enough for clear signals of reorganization events to remain. \citeauthor{BackgroundArticle} analyzed dosage effects after the Ss4r event. They identified 30 ohnologue pairs of genes where one of the copies was specifically up-regulated in the liver. They found that this copy had a higher number of liver-specific transcription factors bound to the promoter regions for these genes compared to their ohnologue counterparts. Examining further, \citeauthor{BackgroundArticle} found that the transcription factor binding sites were overlapping with transposable elements. They speculated if the transposable elements could be acting as promoters for these up-regulated pairs. In this thesis, I will experimentally test if transposable elements in salmon can act as promoters for neighbouring genes, and which transcription factor binding sites are responsible for this up-regulation. 
 
A short divergence will now be taken to explain some elemental concepts of molecular biology and genetics. This is to provide further explanation of some of the principles mentioned above, and furthermore to lay the groundworks for the rest of the thesis.

\subsection{Transposable Elements}
Transposable elements (\ac{TE}) , or "jumping genes" are sequences of DNA that can excise themselves from their original location in the DNA and insert themselves into a target site. They do this via two different mechanisms. The first mechanism is generally referred to as "cut-and-paste" while the second mechanism is considered a "copy-and-paste" mechanism. "Cut-and-paste" transposons are also referred to as Class II transposons, or DNA transposons. This thesis will only be discussing Class II transposons. 

Transposable elements, or transposons, make up about 50\% of the human genome, and are ubiquitous in all eukaryotes. They are considered to be closely related to viruses, and may have been the origin of the first viruses that came into existence. A transposon contains two main sequences of DNA that are vital for its transposition. The first is a transposase gene. The transposase gene is transcribed to produce a transposase enzyme. The transposase will then recognize the TE, excise it from the DNA and insert it into a potential target site. The second main sequences are the terminal inverted repeats(\ac{TIR}s), located on both ends of the TE. TIRs act as recognition sites and inform the transposase where to cut the DNA to excise the transposon. Transposons that contain these two elements are called autonomous transposons, and are capable of transposition on their own without any assistance from outside sources. Non-autonomous transposons may have a mutation in one of these critical elements, that has caused it to become disfunctional and therefore a non-autonomous transposon cannot transpose on its own. 

For transposition events to be inherited, they must take place in the germline. This results in a strong selection pressure for transcription to occur in the gametocytes. It has been observed that many TEs are apparantly restricted to transcribing in the early stages of embryogenesis \citep{Chuong}.

\subsubsection{Tc1-Mariner Transposons}
Tc1-Mariner transposons are a superfamily of TEs that are pervasive in eukaryotes\citep{Classification}, and make up almost 13\% of the salmon genome\citep{Lien}. They generally contain a simple structure of two TIRs at each end and a transposase open reading frame.  (\ac{ORF})\citep{Classification}. The TEs that were examined in this thesis are all from the Tc1-Mariner superfamily. Tc1-Mariners will ideally insert next to TA, which will form TA target site duplications (\ac{TSD})\citep{Classification}.Relatively, this is an extremely general prerequisite for an insertion site, and this may be part of the reason as to why this superfamily is so prolific. 

Trasposition is generally kept at a low level, as insertions can wreak havoc on cellular function, especially the Tc1-Mariner superfamily as their preference for an insertion site is so general. When TEs transpose from one place in the genome to another, they do so selfishly, and not to the benefit of the cell as a whole\citep{Chuong}.It is in the cell's interest to regulate TEs and ensure that they do not transpose too frequently or affect critical genome sequences.

\subsection{Gene Regulation}
This section will cover principles concerning gene regulation, those being 1. Regulatory proteins control gene expression, 2. Transcription factors and their binding sites and 3. Implications on evolutionary molecular processes after WGD. 

\subsubsection{Regulatory proteins control transcription initiation}
Transcription is the process by which the cell converts DNA to RNA. When considering the basics of how transcription functions, the reader should regard DNA as a instruction manual. This instruction manual cannot be damaged under any circumstances. If it would be damaged, the instructions in the manual would be permanently lost. Therefore, the instruction manual is not removed from the home (or in fact, the nucleus). Instead, copies of pages containing specific instructions are made. These copies are in the form of RNA, and they can be transferred outside of the nucleus to then carry out the set of contained instructions. The actual instruction manual remains safely inside the cell. RNA would be the copies of the pages in the manual that can be taken outside of the nucleus. Now, determining which instructions to copy (transcribe into RNA) is the role of gene regulation. 

Gene regulation is the process that allows for multicellular organism to form complex structures and different cell types, all while cells in different tissues contain identical genetic information. Gene regulation allows for cells to differentiate and specialize. Gene expression, or gene regulation in its most basic form is the cell sending signals of which genes are to undergo transcription.  Throughout the process of transcription, there are many different stages where the cell can specify what RNA sequence should be produced. The most pervasively regulated step however, is transcription initiation. Transcription initiation is when RNA polymerase starts transcribing DNA into RNA binds to a promoter on the DNA and starts transcribing a gene into RNA. A promoter, is the DNA sequence that the RNA polymerase originally binds to. In a situation where there are no regulatory aspects involved, a promoter will bind RNA polymerase weakly. Once bound, RNA polymerase will initiate transcription. This would give a low level of constitutive expression, defined as the basal expression. If regulatory factors are involved however, the rate of basal expression would either increase or decrease. By controlling how RNA polymerase binds to DNA, gene regulation can control precisely which genes are expressed \citep{Textbook}

Transcription initiation is in part regulated by transcription factors (\ac{TF}). TFs are one element involved of the transcription initiation process. A TF is a protein that binds to the DNA, and then allows for the RNA polymerase to bind and transcribe a gene. The RNA polymerase, in most eukaryotes, often cannot bind to the DNA without TFs present. TFs are a necessary step to initiate transcription of genes. 

\subsubsection{Transcription factors and their binding sites }
As mentioned previously, transcription factors bind to DNA to allow the initiation of transcription. TFs bind to transcription factor binding sites (\ac{TFBS}) which are short sequences that the TFs can recognize. It is tempting to assume that TFs randomly interact with the DNA, landing on an area to see if it contains the correct TFBS. A process like this is too inefficient. Instead, TFs interact non-specifically with the DNA backbone, which allows it to sequentially search the DNA for its binding site. The TF travels along the DNA until it encounters its binding site, where it alters its conformation to specifically interact with nucleotides on the interior of the helix. Once bound, the TF can interact with RNA polymerase to initiate transcription. Hence, if TEs in salmon have acquired TFBSs and are acting as promoters, this can have consequences for the entire gene regulatory network in liver cells. 

\citeauthor{BackgroundArticle} stated that many of the TFBSs that surrounded the up-shifted ohnolog copies have a known liver function, such as hepatocyte nuclear factors and roles in lipid metabolism. TFBSs can in theory be accumulated by the insertion of transposable elements (\ac{TE}) carrying TFBSs. Moreover, TEs can attain mutations that form novel TFBSs. If this is the case, the TEs are acting as regulomic tools determining if neighbouring genes to a potential insertion site should be up-shifted or left untouched. 

\subsubsection{Promoters and Enhancers}
There are numerous elements that make up the complex regulatory networks in DNA. Promoters and enhancers are two such regulatory elements. This thesis will discuss both promoters and enhancers. When promoters are mentioned, this is a general reference to a regulatory element that is located relatively near the affected gene. There are two promoters in question. One is the stereotypical idea of a promoter. This is a specific sequence that the RNA polymerase binds to and where initiation of transcription takes place. For clarity's sake, the term "primary promoter" will be used when referring to this specific type of promoter in this thesis. There is a second type of promoter however. This kind of promoter is a "secondary promoter"; an area occupied by transcription factor binding sites that attract an RNA polymerase to the primary promoter. A primary promoter can also be occupied by transcription factors and does not require a secondary promoter to function. These terms are defined to provide clarity when discussing the TEs in this thesis. The TEs examined in this thesis to be acting as promoters, are acting as secondary promoters, not primary promoters. 

Enhancers are very similar to promoters and the two terms are often intertwined. However, while a promoter is ordinarily located near the affected gene, an enhancer can regulate a gene distally. Enhancers can regulate genes that are located thousands if not millions of base pairs away, and can even affect  $trans$-elements. Such regulatory elements form incredibly complex networks that pose a challenge to study. This is what makes the timing of the Ss4r WGD event unique, and Atlantic salmon an ideal model organism for genetic research. 

\subsection{Salmonids}
In this section, a brief description of the salmonid family and genome is given and the conditions that make Atlantic salmon ($Salmo$ $salar$) an ideal model organism for studying the evolutionary adaptions occuring after WGD. 
Firstly, the Atlantic Salmon genome has over 10 genome assemblies, and 100s of RNAseq datasets. \citep{Lien} They have close outgroups like Northern Pike to provide a comparison for genomic research. The salmonid evolutionary lineage is visualized in Figure \ref{fig:SsalEle0256}. 

Most importantly, salmonids have a relatively recent WGD. This is unique, as other WGD such as 1R, 2R and Ts3R occurred so long ago that there are few clear markers of post-WGD genomic reorganization events to study. Therefore,very little known about the mechanistic processes of chromosomal reorganization.  The fourth  WGD occured around 100-80 million years ago and is referred to by the name Ss4r (Salmonid-specific Fourth Vertebrate Whole-Genome Duplication). This time bracket is an evolutionary "sweet spot" as it is short enough to observe the after-effects of a WGD event, but also long enough to view genomic recovery and consequences of a WGD.  Ss4r was an autopolyploidization, meaning the entire genome went from containing 25 chromosomes to containing 50 chromosomes\citep{Lien}. This would have been incredibly stressful for the cells. When a cell experiences environmental stressors, their regulation of TEs is partly relinquished. Higher levels of transposition have been observed after WGD, which can arguably be called an environmental stressor. The WGD event Ss4r could have triggered a higher level of transposition in the genome due to cellular stress. If this is the case, Tc1-Mariners containing liver-specific transcription factor binding sites could have inserted near genes in one of the ohnologue pairs, but not the other. This would result in the up-regulation of one of the gene copies, and consequently the TEs would act as promoters of those genes.

 \section{Aims and Objectives}
The aim of this thesis is to test whether transposable elements in $Salmo$ $salar$ act as promoters of neighbouring genes. Furthermore, whether liver-specific transcription factor binding sites overlapping with transposable elements are directly impacting the regulation of neighbouring genes, and therefore determining if transposable elements can act as regulatory elements. 

\section{Hypothesis}

 
 
 
\chapter{Materials and Method}

\section{Overview}
- phylogenetics
- First we isolated the DNA from a salmon embryo, then we amplified the transposon sequences using PCR.
- we transformed plasmids to ensure that we got identically genetic plasmids from single colonies to use as cloning templates
- then we cloned the amplicons into the plasmids, and meausured the photowaves emitted from the luciferase to determine if the transposons increase transcription of the luciferase gene. 
- simultaneously in the same cells, the photowaves from first a negative control and then a positive control were measured, It took some time to determine the correct ratios between teh controls and the experimental plasmids. 

\section{Phylogenetics}
We first created a phylogenetic tree from the Tc1 mariner database (Figure). This allowed us to identify which transposons were the most relevant and had the highest correlation to gene expression level changes. There were 8 transposons that fit these criteria. 2 of these were under 100 base pairs long, and were therefore not included in this project. The remaining 6 were aligned with the closest consensus sequence. 5 out of 6 alignments were GOOD, (NC... NC... NC... NC... and NC...). The TFBSs on these 5 transposons were identified, and are shown in Figure \ref{fig:TFBSalignment}. Most of the TFBSs are fairly well conserved. 








\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{Figures/Three_Part_Alignment.png}
    \caption[TFBS Alignment]{TFBS Alignment: Here the TFBSs MA, MA and MA are shown in their alignment positions.}
    \label{fig:TFBSalignment}
\end{figure}


\section{Salmonid DNA Isolation}
This section will describe the procedure for isolating DNA from dechlorinated Atlantic Salmon embryos. The embryos were harvested, hatched and frozen by Doctor.Alex Kojo Datsomer. DNA was isolated using the DNeasy Blood and Tissue Kit. Several modifications were applied to the protocol for this kit, which are discussed below. 

Beads were used to crush the fish embryos into powder. These beads had to be sterilized beforehand as to not contaminate the samples. This was done by preparing a 0.4M HCl solution, which was then used to rinse the beads. Before use, the sterilized beads were placed into a freezer for 30 minutes, to lower their temperature to -80\degree C. A TissueLyser block was also placed in the freezer at -80\degree C. Consequently, the temperature of both the block and the beads will be equal to that of the sample tissues so the tissues do not thaw prior to adding the Buffer ATL. This is a modification to the original DNeasy Blood and Tissue Kit protocol, where it is instructed to thaw the tissue samples prior to carrying out the protocol. However, the enzymes DNase and RNase are active at room temperature and start to break down DNA (reference). Therefore, it was undesirable to allow the samples to thaw to room temperature and for these enzymes to start breaking down the DNA present in the samples. Hence, the samples were kept frozen until they reacted with Buffer ATL. \citep{DNeasyHandbook}


Dechlorinated Atlantic salmon embryos were used as the sample tissues for DNA isolation. Samples 1 and 8 from Doctor Alex Kojo Datsomer were acquired for this purpose. The embryos were placed in a TissueLyser block along with 1 bead in each sample tube. The TissueLyser was then run for 2 minutes at a frequency of 30/second. Then the sample tubes were quickly placed back on ice. 

While working in a fume hood, 360$\mu L$  of Buffer ATL was transferred to both samples, and immediately vortexed for 30 seconds. This is twice the amount stated in the kit protocol. This was done as the embryos were rather large, and therefore Buffer ATL, Proteinase K, Ethanol and Buffer AL were all doubled. The top of the tube was vortexed as well, ensuring that no embryonic remains were left uncombined. After this, the samples were kept at room temperature. 

4 $\mu L$ of Proteinase K was added to the samples. This was done by mistake, as 40$\mu L$ was supposed to be added. As this mistake was not noticed until later, the procedure was run to completion and DNA was isolated dispite the small amount of Proteinase K. Therefore, this modification was added as it is a more economical use of Proteinase K. The samples were vortexed for 15 seconds. The samples were then incubated at 56\degree C for 40 minutes. The samples were vortexed for 15 seconds at the 20 minute mark. 

The samples were vortexed for 15 seconds after incubation was complete. 400 $\mu L$ of Buffer AL was transferred to each sample. The samples were vortexed for 15 seconds, and then left to incubate at 56 \degree C for 10 minutes. 400 $\mu L$ of 100\% ethanol was then added. The samples were vortexed for 15 seconds to ensure thurough mixing. 

Half of the mixtures were pipetted into DNeasy spin columns, which were then placed in 1.5$mL$ collection tubes. This was done sequentially in two parts, as the mixtures were too large for the DNeasy spin columns. The samples were then centrifuged at 8000rpm (6000\geqslant g) for 1 minute. The flow-through was discarded, and the collection tubes were dried off on a piece of paper. The spin column was then placed back into the collection tube. This was seen as more economical than the protocol's recommendation, which is to replace the collection tubes. The other half of the mixtures were added to the spin columns, and the samples were again centrifuged at 8000rpm (\geqslant 6000) for 1 minute. The flow-through was discarded and the collection tubes were dried off in the same manner as before. The spin columns were placed back into the collection tubes. 

500$\mu L$ of Buffer AW1 was transferred to each spin column and centrifuged for 1 minute at 8000rpm. The flow-through was discarded and the collection tube was dried off as before. The spin columns were placed back into the collection tubes. 500$\mu L$ of Buffer AW2 was transferred to each spin column and centrifuged for 3 minutes at 14,000rpm (20,000 $\times g$). The flow-through was discarded and the collection tube was dried off as before. The spin columns were placed back into the collection tubes. The samples were then centrifuged for 1 more minute at 14,000rpm to ensure that all debris had passed through the membrane of the spin column, and that the resulting DNA would be uncontaminated. The flow-through and the colleciton tubes were discarded, and the spin columns were transferred to new 1.5 $\mu L$ collection tubes. The DNA was eluted by adding 200 $\mu L$ of distilled water to the center of the spin column membrane. The samples were then incubated for 3 minutes at room temperature. The protocol requires 1 minute of incubation, however it was desireable to provide a longer amount of time for the elution to be carried out. The samples were then centrifuged at 14000rpm. The protocol calls for 8000rpm, however it was desired to extract as much DNA as possible and therefore a higher rpm was set. The spin columns were then discarded, and the DNA in the tubes was analyzed using a ... The tubes were labeled with the sample number, and the $ng/\mu L$ DNA present in each tube. The tubes were then stored in a -20\degree C freezer. 


\section{Transformation of Plasmids}

This section will cover step-by-step how the transformations of pGL3$\_$fLuc plasmid and pGL4.75$\_$rHLuc plasmid were carried out. 

Agar plates were prepared beforehand, ..

Identical procedures were carried out for the renilla plasmid and the firefly plasmid. 

There are going to be two different controls for this experiment. The first is going to be a renilla plasmid control. This is to provide a general comparioson as to whether the TEs have increased expression compared to a CMV enhancer. 
The second control will be the CMV enhancer cloned into the firefly plasmid, to provide a positive control. 

The plasmid used in this transformation was pGL4.75_rHLuc plasmid from ..., and the competent cells used were dH5$\alpha$ competent cells. Both the plasmid and the bacteria are stored in -80 \degree C when not in use. They are thawed on ice for 30 minutes before the transformation protocol was carried out. 

The following procedure was carried out in a sterile hood where possible.3$\mu L$ of plasmid was transferred to 50$\mu L$ of the competent cells. The resulting mixture was gently mixed by pipetting. The mixture was then incubated on ice for 30 minutes. After incubation, the cells were heat shocked for exactly 45 seconds at 42\degree C in a water bath, and then immediately placed back on ice. The mixture was placed on ice for a total of 2 minutes. Then the cells were incubated at 37\degree C at 220rpm for 1 hour in a ... An agar plate was also incubated at 37\degree C. the plate was placed upside down as to prevent condensation from contaminating the agar medium. When incubation was completed, 50$\mu L$ of the mixture was pipetted onto the agar plate, and spread using a blue stick. The agar plate was then incubated overnight. Incubated started at 15:23 and ended at 9:30 the next day. 

The agar plate was sealed with parafilm and left in a -4\degree refridgeration unit until later in the day. 

A LB Broth Base Medium was prepared using the following procedure...

The following procedure was done in a sterile hood. 
20$mL$ LB Broth Base Medium and 20$\mu L$ ampicillin were combined in a 50$mL$ tube.  A 20 $\mu L$ pipette was wiped clean using 70\% ethanol. A sterile pipette tip was used to scrape off a single colony off of the agar plate, and the tip was released into the LB Broth Base Medium and Ampicillin mixture. The sample was then sealed and placed in an incubation machine overnight, at 37\degree C and 220rpm. A new LB Broth Base and ampicillin mixture was prepared, and a second colony was sampled to ensure that there would be enough viable plasmid for cloning. Incubation started at 13:30 and ended at 9:30 the next day.

The two samples were centrifuged for 5 minutes at 4000rpm at 4\degree C. The supernatant was removed by pouring it over into a bacterial waste flask.  

smart to BLAST them first. TE
For each TE that has a motif, you have BLASTED them to the consensus sequence. Can happent eh BLAST sequence is reverse complementary. 

\section{Infusion HD Cloning} 
Primers were designed using In-Fusion Cloning Primer Design Tool (reference). The primers used for the different transposons are shown in Figure \ref{fig:SsalEle0256},  
The Infusion HD Cloning Method was chosen because it does not require ligation or restriction enzymes. This was ideal as the different transposons carried similar restriction sites as those on the plasmids, 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\linewidth]{Figures/fLuc_Ssalle0180.png}
    \caption[SsalEle0180_Map]{Transposon SsalEle0180 in Firefly Plasmid: This is a map showing the SsalEle0256 transposon after it has been cloned into the Firefly plasmid.}
    \label{fig:SsalEle0256}
\end{figure}

\chapter{Results}

\chapter{Discussion}


\appendix
\begin{landscape}
\begin{table}
\centering
\begin{tabular}{|r|l|r|r|r|r|l|l|l|l|l} 
\hline
              & TFBS\_chrom    & TFBS\_start       & TFBS\_end         & gene\_id           & TE            & TE\_chrom      & TE\_start         & TE\_stop          & TE\_long                                                               & TE\_fam                 \\ 
\hline
4             & ssa03          & 23524928          & 23524941          & \textbf{106599366} & \textbf{1.00} & \textbf{ssa03} & \textbf{23524913} & \textbf{23525122} & \textbf{ID=Ssa\_epeat\_231255;Name=DTT\_salEle0199;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
5             & ssa03          & 23524928          & 23524941          & 106599366          & 1.00          & ssa03          & 23524913          & 23525122          & ID=Ssa\_epeat\_231255;Name=DTT\_salEle0199;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
6             & ssa03          & 23525080          & 23525091          & 106599366          & 1.00          & ssa03          & 23524913          & 23525122          & ID=Ssa\_epeat\_231255;Name=DTT\_salEle0199;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
7             & ssa03          & 23525080          & 23525091          & 106599366          & 1.00          & ssa03          & 23524913          & 23525122          & ID=Ssa\_epeat\_231255;Name=DTT\_salEle0199;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
\textbf{403}  & \textbf{ssa06} & \textbf{63241962} & \textbf{63241970} & \textbf{106607938} & \textbf{1.00} & \textbf{ssa06} & \textbf{63240440} & \textbf{63242031} & \textbf{ID=Ssa\_epeat\_76305;Name=DTT\_salEle0256;Note=DNA/TcMar-Tc1}  & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{404}  & \textbf{ssa06} & \textbf{63241964} & \textbf{63241981} & \textbf{106607938} & \textbf{1.00} & \textbf{ssa06} & \textbf{63240440} & \textbf{63242031} & \textbf{ID=Ssa\_epeat\_76305;Name=DTT\_salEle0256;Note=DNA/TcMar-Tc1}  & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{405}  & \textbf{ssa06} & \textbf{63241971} & \textbf{63241990} & \textbf{106607938} & \textbf{1.00} & \textbf{ssa06} & \textbf{63240440} & \textbf{63242031} & \textbf{ID=Ssa\_epeat\_76305;Name=DTT\_salEle0256;Note=DNA/TcMar-Tc1}  & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{418}  & \textbf{ssa09} & \textbf{24004499} & \textbf{24004515} & \textbf{106611011} & \textbf{1.00} & \textbf{ssa09} & \textbf{24002910} & \textbf{24004524} & \textbf{ID=Ssa\_epeat\_22966;Name=DTT\_salEle0180;Note=DNA/TcMar-Tc1}  & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
678           & ssa10          & 7272478           & 7272491           & 106613481          & 1.00          & ssa10          & 7272475           & 7272494           & ID=Ssa\_epeat\_760376;Name=DTT\_salEle0709;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
679           & ssa10          & 7272487           & 7272498           & 106613481          & 1.00          & ssa10          & 7272475           & 7272494           & ID=Ssa\_epeat\_760376;Name=DTT\_salEle0709;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
\textbf{740}  & \textbf{ssa10} & \textbf{7297223}  & \textbf{7297234}  & \textbf{106613481} & \textbf{1.00} & \textbf{ssa10} & \textbf{7296152}  & \textbf{7297281}  & \textbf{ID=Ssa\_epeat\_760438;Name=DTT\_salEle0351;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{806}  & \textbf{ssa10} & \textbf{7318592}  & \textbf{7318606}  & \textbf{106613481} & \textbf{1.00} & \textbf{ssa10} & \textbf{7318227}  & \textbf{7319384}  & \textbf{ID=Ssa\_epeat\_760485;Name=DTT\_salEle0401;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{807}  & \textbf{ssa10} & \textbf{7318622}  & \textbf{7318636}  & \textbf{106613481} & \textbf{1.00} & \textbf{ssa10} & \textbf{7318227}  & \textbf{7319384}  & \textbf{ID=Ssa\_epeat\_760485;Name=DTT\_salEle0401;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{808}  & \textbf{ssa10} & \textbf{7318633}  & \textbf{7318647}  & \textbf{106613481} & \textbf{1.00} & \textbf{ssa10} & \textbf{7318227}  & \textbf{7319384}  & \textbf{ID=Ssa\_epeat\_760485;Name=DTT\_salEle0401;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{809}  & \textbf{ssa10} & \textbf{7318692}  & \textbf{7318707}  & \textbf{106613481} & \textbf{1.00} & \textbf{ssa10} & \textbf{7318227}  & \textbf{7319384}  & \textbf{ID=Ssa\_epeat\_760485;Name=DTT\_salEle0401;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
1036          & ssa17          & 5354900           & 5354914           & 106575074          & 1.00          & ssa17          & 5354893           & 5354974           & ID=Ssa\_epeat\_353255;Name=DTT\_salEle0150;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
1037          & ssa17          & 5354901           & 5354916           & 106575074          & 1.00          & ssa17          & 5354893           & 5354974           & ID=Ssa\_epeat\_353255;Name=DTT\_salEle0150;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
1038          & ssa17          & 5354941           & 5354951           & 106575074          & 1.00          & ssa17          & 5354893           & 5354974           & ID=Ssa\_epeat\_353255;Name=DTT\_salEle0150;Note=DNA/TcMar-Tc1          & DNA/TcMar-Tc1           \\ 
\cline{1-10}
\textbf{1356} & \textbf{ssa25} & \textbf{32494951} & \textbf{32494966} & \textbf{106586620} & \textbf{1.00} & \textbf{ssa25} & \textbf{32494034} & \textbf{32495493} & \textbf{ID=Ssa\_epeat\_546399;Name=DTT\_salEle0709;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{1357} & \textbf{ssa25} & \textbf{32495016} & \textbf{32495031} & \textbf{106586620} & \textbf{1.00} & \textbf{ssa25} & \textbf{32494034} & \textbf{32495493} & \textbf{ID=Ssa\_epeat\_546399;Name=DTT\_salEle0709;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{1358} & \textbf{ssa25} & \textbf{32495017} & \textbf{32495030} & \textbf{106586620} & \textbf{1.00} & \textbf{ssa25} & \textbf{32494034} & \textbf{32495493} & \textbf{ID=Ssa\_epeat\_546399;Name=DTT\_salEle0709;Note=DNA/TcMar-Tc1} & \textbf{DNA/TcMar-Tc1}  \\ 
\cline{1-10}
\textbf{1359} & \textbf{ssa25} & \textbf{32495063} & \textbf{32495069} & \textbf{106586620} & \textbf{1.00} & \textbf{ssa25} & \textbf{32494034} & 32495493          & ID=Ssa\_epeat\_546399;Name=DTT\_salEle0709;Note=DNA/TcMar-Tc1          & \textbf{DNA/TcMar-Tc1}  \\
\hline
\end{tabular}
\end{table}
\end{landscape}

\bibliography{references}
\end{document}
